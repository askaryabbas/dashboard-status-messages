<?php

/**
 * Dashboard Status Messages Plugin
 *
 * @package           DSM
 * @author            Askary Abbas
 * @copyright         2022 WPMU DEVELOPMENT
 * @license           GPL-2.0-or-later
 *
 * @wordpress-plugin
 * Plugin Name:       Dashboard Status Messages
 * Plugin URI:        https://example.com/dashboard-status-messages
 * Description:       Allow posting a status message on the WordPress admin dashboard page.
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.4
 * Author:            Askary Abbas
 * Author URI:        https://example.com
 * Text Domain:       dashboard-status-messages
 * License:           GPL v2 or later
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Update URI:        https://example.com/dashboard-status-messages/
 * Domain Path:       /languages
 */

// Restrict Direct Access
if ( ! defined( 'WPINC' ) ) {
    die;
}

if ( ! defined( 'WPMU_DSM_VERSION' ) ) {
    define( 'WPMU_DSM_VERSION', '1.0.0' );
}

if ( ! defined( 'WPMU_DSM_BASENAME' ) ) {
    define( 'WPMU_DSM_BASENAME', plugin_basename(__FILE__) );
}

if ( ! defined( 'WPMU_DSM_DIR' ) ) {
    define( 'WPMU_DSM_DIR', plugin_dir_path(__FILE__) );
}

if ( ! defined( 'WPMU_DSM_URL' ) ) {
    define( 'WPMU_DSM_URL', plugin_dir_url(__FILE__) );
}

/**
 * The core plugin class is used to define internationalization, rendering views and
 * admin-specific hooks in this plugin.
 */
require_once WPMU_DSM_DIR . 'controller/class-dashboard-status-messages.php';

if ( ! function_exists( 'wpmu_dashboard_status_messages' ) ) {

    /**
     * Main instance of plugin.
     * @return Dashboard_Status_Messages
     */
    function wpmu_dashboard_status_messages() {
        return \DSM\DSM_Controller\Dashboard_Status_Messages::get_instance();
    }
}

// Init the plugin and load the plugin instance for the first time.
add_action( 'plugins_loaded', 'wpmu_dashboard_status_messages' );
