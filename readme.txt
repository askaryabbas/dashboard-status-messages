﻿=== Dashboard Status Messages ===
Contributors: askaryabbas
Donate link: https://example.com/
Tags: statusmassages, status, messages, notice, notices
Requires at least: 5.2
Tested up to: 7.4
Stable tag: 1.0.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Allow posting a status message on the WordPress admin dashboard page.

== Description ==

This Plugin will allow posting a status message on the WordPress admin dashboard page on the admin notices and dashboard widgets.

== Installation ==

This section describes how to install the plugin and get it working.

e.g:

1. Download the zip file and extract it.
2. Upload `dashboard-status-messages` directory to the `/wp-content/plugins/` directory
3. Activate the plugin through the \'Plugins\' menu.
4. Alternatively you can use WordPress Plugin installer from Dashboard->Plugins->Add New to add this plugin
5. Enjoy

== Frequently Asked Questions ==

= Does This plugin requires any other plguin =
No

== Screenshots ==

1. It is displaying Plugin dashboard page for main site and single sites, Plugin sub-menu under Tools screenshot-1.png
2. It is displaying Plugin dashboard for network admin, Plugin dashboard page on the main menu screenshot-2.png
3. It is displaying Status message published both as an admin notice and dashboard widgets screenshot-3.png

== Changelog ==

= 1.0.0 =
* first version.