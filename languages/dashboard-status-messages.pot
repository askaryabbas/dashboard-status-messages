# Copyright (C) 2022 Askary Abbas
# This file is distributed under the GPL v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Dashboard Status Messages 1.0.0\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/dashboard-status-messages\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2022-03-12T09:49:41+00:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"X-Generator: WP-CLI 2.6.0\n"
"X-Domain: dashboard-status-messages\n"

#. Plugin Name of the plugin
#: controller/class-dashboard-status-messages.php:150
#: controller/class-dashboard-status-messages.php:159
#: views/admin/status-management-page.php:9
msgid "Dashboard Status Messages"
msgstr ""

#. Plugin URI of the plugin
msgid "https://example.com/dashboard-status-messages"
msgstr ""

#. Description of the plugin
msgid "Allow posting a status message on the WordPress admin dashboard page."
msgstr ""

#. Author of the plugin
msgid "Askary Abbas"
msgstr ""

#. Author URI of the plugin
msgid "https://example.com"
msgstr ""

#: controller/class-dashboard-status-messages.php:151
#: controller/class-dashboard-status-messages.php:160
msgid "Status Messages"
msgstr ""

#: controller/class-dashboard-status-messages.php:196
msgid "Settings"
msgstr ""

#: views/admin/status-management-page.php:14
msgid "Status updated."
msgstr ""

#: views/admin/status-management-page.php:20
msgid "Add status message to show on the admin notice and the dashboard page."
msgstr ""

#. translators: Warning text in html tag.
#: views/admin/status-management-page.php:28
msgid "%s You are network admin, you have ability to override all the sites status messages by submitting this form."
msgstr ""
