<?php
// Restrict Direct Access
if ( ! defined( 'WPINC' ) ) {
    die;
}
?>
<div class="dsm-widget dsm-status-message">
    <span class="dashicons-before dashicons-megaphone"></span>
    <div class="dsm-status-message-content">
        <?php do_action( 'dsm_before_dashboard_widget_content' ); ?>
        <?php echo wpautop( wp_kses( $notice, $editor_data['allowed_html'] ) ); ?>
        <?php do_action( 'dsm_after_dashboard_widget_content' ); ?>
    </div>
</div>