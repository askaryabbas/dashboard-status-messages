(function ($) {
    // Enable strict mode.
    'use strict';

    $(function () {
        window.wp = window.wp || {};
    });
})(jQuery, window, document);