<?php
// Restrict Direct Access
if ( ! defined( 'WPINC' ) ) {
    die;
}
?>
<div class="wrap" id="dashboard-status-menu">
    <h1><?php esc_html_e( 'Dashboard Status Messages', 'dashboard-status-messages' ); ?></h1>
    <?php settings_errors(); ?>
    <?php if ( isset( $_GET[ 'updated' ] ) && 'true' == $_GET[ 'updated' ] ) : ?>
        <div class="notice notice-success is-dismissible">
            <p><?php echo esc_html__( 'Status updated.', 'dashboard-status-messages' ); ?></p>
        </div>
    <?php endif; ?>
    <div class="dashboard-status-menu-form-wrap">
        <?php do_action( 'dsm_before_dashboard_editor_form' ); ?>
        <form action="<?php echo esc_url( $editor_data['dsm_page_url'] ); ?>" method="post">
            <h2 class="status-message-form-title">
                <?php echo esc_html__( 'Add status message to show on the admin notice and the dashboard page.', 'dashboard-status-messages' ); ?>
            </h2>
            <?php if ( is_network_admin() ): ?>
                <h2 class="status-message-form-title-network">
                    <span class="dashicons-before dashicons-warning"></span><span class="dsm-network-admin-warning">
                        <?php
                        printf(
                                /* translators: Warning text in html tag. */
                                esc_html__( '%s You are network admin, you have ability to override all the sites status messages by submitting this form.', 'dashboard-status-messages' ),
                                '<strong>Warning: </strong>'
                        );
                        ?>
                    </span>
                </h2>
            <?php endif; ?>
            <?php do_action( 'dsm_before_dashboard_editor' ); ?>
            <?php wp_editor( $editor_data['content'], $editor_data['editor_id'], $editor_data['editor_args'] ); ?>
            <?php do_action( 'dsm_after_dashboard_editor' ); ?>
            <?php submit_button( 'Update Status' ); ?>
            <?php wp_nonce_field( 'status-messages-page-save', 'status-messages-page-save-nonce' ); ?>
        </form>
        <?php do_action( 'dsm_after_dashboard_editor_form' ); ?>
    </div>
</div>