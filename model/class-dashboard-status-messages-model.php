<?php

/**
 * Dashboard_Status_Messages_Model model class.
 *
 * @package DSM_Model
 */

namespace DSM\DSM_Model;

// Restrict Direct Access
if ( ! defined( 'WPINC' ) ) {
    die;
}

class Dashboard_Status_Messages_Model {

    /**
     * Plugin instance
     *
     * @var null
     */
    private static $instance = null;

    /**
     *  Network
     *
     * @var false
     */
    protected $is_network = false;

    /**
     *  Network admin
     *
     * @var false
     */
    protected $is_network_admin = false;

    /**
     *  Blog ID
     *
     * @var default current 1
     */
    protected $current_blog_id = 1;

    /**
     * Dashboard_Status_Messages_Model constructor
     */
    public function __construct() {

        if ( is_multisite() ) {
            $this->is_network = true;
        }

        if ( is_multisite() && is_network_admin() ) {
            $this->is_network_admin = true;
        }
        $this->current_blog_id = get_current_blog_id();
    }

    /**
     * Return the plugin instance
     *
     * @return Dashboard_Status_Messages_Model
     */
    public static function get_instance() {
        if ( ! self::$instance ) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Return wp_editor setting arguments and content
     *
     * @return array
     */
    public function status_message_editor( $notice_content ) {

        $content = isset( $notice_content ) ? $notice_content : '';
        
        // Get base url.
        $base_url = $this->is_network_admin ? network_admin_url( 'admin.php' ) : admin_url( 'tools.php' );

        // Get page.
        $tools_url = add_query_arg(
                array(
                    'page' => 'dashboard-status-messages',
                    'updated' => true
                ),
                $base_url
        );

        $allowed_html = array(
            'a' => array(
                'href' => true,
                'title' => true,
                'target' => true,
                'alt' => true,
            ),
            'b' => array(),
            'strong' => array(),
            'i' => array(),
            'em' => array(),
            'del' => array(),
            'br' => array(),
            'p' => array(),
            'ol' => array(),
            'ul' => array(),
            'li' => array()
        );
        
        // Filter editor allowed HTML tags.
        $allowed_html = apply_filters('dsm_editor_allowed_html_tags', $allowed_html);
        
        $editor_args = array(
            'tinymce' => array(
                'toolbar1' => 'undo redo | bold italic',
                'toolbar2' => '',
                'toolbar3' => '',
                'width' => 800,
                'height' => 300
            ),
            'media_buttons' => false,
            'quicktags' => false,
            'textarea_name' => 'dsm_status_message'
        );
        
        // Filter editor arguments.
        $editor_args = apply_filters('dsm_editor_arguments', $editor_args);
        
        $editor_data = array(
            'editor_args' => $editor_args,
            'editor_id' => 'status-messages',
            'content' => $content,
            'dsm_page_url' => $tools_url,
            'allowed_html' => $allowed_html
        );

        // Filter editor data, allowed HTML etc.
        $editor_data = apply_filters('dsm_editor_data', $editor_data);
        
        return $editor_data;
    }

    /**
     * The method for saving the options to the database or for deleting them
     * based on what the user has specified on the submenu page.
     *
     * @since    1.0.0
     */
    public function save_setting_messages_option() {
        
        $action = 'status-messages-page-save';
        $nonce = 'status-messages-page-save-nonce';

        // If the user doesn't have permission to save, nonce check
        if ( ! $this->user_can_save( $action, $nonce ) ) {
            return false;
        }

        // Make sure we have data in POST request.
        if ( ! isset( $_POST['dsm_status_message'] ) ) {
            return false;
        }

        $unfiltered_content = isset( $_POST['dsm_status_message'] ) ? $_POST['dsm_status_message'] : '';

        $allowed_html = array(
            'a' => array(
                'href' => true,
                'title' => true,
                'target' => true,
                'alt' => true,
            ),
            'b' => array(),
            'strong' => array(),
            'i' => array(),
            'em' => array(),
            'del' => array(),
            'br' => array(),
            'p' => array(),
            'ol' => array(),
            'ul' => array(),
            'li' => array()
        );
        $content = wp_kses( $unfiltered_content, $allowed_html );

        if ( $this->is_network_admin ) {
            $blog_ids = get_sites( array( 'fields' => 'ids' ) );
            foreach ( $blog_ids as $blog_id ) {
                update_blog_option( $blog_id, 'dsm_status_message', $content );
            }
        } else {
            self::is_network_active() ? update_blog_option( $this->current_blog_id, 'dsm_status_message', $content ) : update_option( 'dsm_status_message', $content );
        }

        /**
         * Action hook to execute after updating dsm settings.
         *
         * @param array $options old values.
         *
         * @since 1.0.0
         */
        do_action( 'status_message_setting_updated', $content );
    }

    /**
     * Determines if the user has permission to save the information from the submenu
     * page.
     *
     * @since    1.0.0
     * @access   private
     *
     * @param    string    $action   The name of the action specified on the submenu page
     * @param    string    $nonce    The nonce specified on the submenu page
     *
     * @return   bool                True if the user has permission to save; false, otherwise.
     */
    private function user_can_save( $action, $nonce ) {

        $is_nonce_set = isset( $_POST[ $nonce ] );
        $is_valid_nonce = false;

        if ( $is_nonce_set ) {
            $is_valid_nonce = wp_verify_nonce( $_POST[ $nonce ], $action );
        }

        return ( $is_nonce_set && $is_valid_nonce );
    }

    /**
     * Check if the plugin is active network wide.
     *
     * @since 1.0.0
     *
     * @return mixed|void
     */
    public static function is_network_active() {
        static $active = null;

        if ( is_multisite() ) {
            // Make sure the plugin is defined before use it.
            if ( ! function_exists( 'is_plugin_active_for_network' ) ) {
                require_once ABSPATH . '/wp-admin/includes/plugin.php';
            }

            $active = is_plugin_active_for_network( WPMU_DSM_BASENAME );
        } else {
            $active = false;
        }

        return $active;
    }

}