<?php

/**
 * Dashboard_Status_Messages main loader class.
 *
 * @package DSM_Controller
 */

namespace DSM\DSM_Controller;

use DSM\DSM_Model;

// Restrict Direct Access
if ( ! defined( 'WPINC' ) ) {
    die;
}

class Dashboard_Status_Messages {

    /**
     * Plugin instance
     *
     * @var null
     */
    private static $instance = null;

    /**
     * Plugin instance
     *
     * @var null
     */
    protected $model_instance = null;

    /**
     *  Network
     *
     * @var false
     */
    protected $is_network = false;

    /**
     *  Blog ID
     *
     * @var default current 1
     */
    protected $current_blog_id = 1;

    /**
     *  Network admin
     *
     * @var false
     */
    protected $is_network_admin = false;

    /**
     *  Has notice content
     *
     * @var false
     */
    protected $has_notice = false;

    /**
     * Dashboard_Status_Messages constructor
     */
    public function __construct() {

        if ( is_multisite() ) {
            $this->is_network = true;
        }

        if ( is_multisite() && is_network_admin() ) {
            $this->is_network_admin = true;
        }

        $this->current_blog_id = get_current_blog_id();

        if ( is_admin() ) {
            $this->model();
            $this->init();
            $this->laod_textdomain();
        }
    }

    /**
     * Return the plugin instance
     *
     * @return Dashboard_Status_Messages
     */
    public static function get_instance() {
        if ( ! self::$instance ) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Plugin Initialization.
     */
    private function init() {
        $notice = $this->model_instance::is_network_active() ? get_blog_option( $this->current_blog_id, 'dsm_status_message' ) : get_option( 'dsm_status_message' );
        $this->has_notice = ! empty( $notice ) ? true : false;
        $network_ = $this->is_network_admin ? 'network_' : '';
        $network_admin_ = $this->is_network_admin ? 'network_admin_' : '';
        add_action( "{$network_}admin_menu", array( $this, 'admin_menu' ) );
        add_action( "{$network_admin_}plugin_action_links_" . WPMU_DSM_BASENAME, array( $this, 'add_plguin_setting_link' ) );
        add_action( 'load-tools_page_dashboard-status-messages', array( $this->model_instance, 'save_setting_messages_option' ) );
        add_action( 'load-toplevel_page_dashboard-status-messages', array( $this->model_instance, 'save_setting_messages_option' ) );
        add_action( 'admin_notices', array( $this, 'admin_notice' ) );
        add_action( 'network_admin_notices', array( $this, 'admin_notice' ) );
        if ( $this->has_notice ) {
            add_action( "wp_{$network_}dashboard_setup", array( $this, 'add_dashboard_widgets' ) );
        }
        add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_styles' ) );
        add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

        // Triggered when DSM is totally loaded.
        do_action( 'dsm_loaded' );
    }

    /**
     * Load model
     * 
     * @since 1.0.0
     */
    private function model() {
        //Loading model.
        require_once WPMU_DSM_DIR . 'model/class-dashboard-status-messages-model.php';
        $this->model_instance = DSM_Model\Dashboard_Status_Messages_Model::get_instance();
    }

    /**
     * Load Translations
     * 
     * @since 1.0.0
     */
    private function laod_textdomain() {
        load_plugin_textdomain( 'dashboard-status-messages', false, 'dashboard-status-messages/languages/' );
    }

    /**
     * Register admin submenu under tools for all websites except network admin.
     * Network admin have specific menu to override all the status messages.
     * 
     * @since 1.0.0
     */
    public function admin_menu() {
        if ( $this->is_network_admin ) {
            add_menu_page(
                    esc_html__( 'Dashboard Status Messages', 'dashboard-status-messages' ),
                    esc_html__( 'Status Messages', 'dashboard-status-messages' ),
                    'manage_network_options',
                    'dashboard-status-messages',
                    array( $this, 'admin_page' ),
                    'dashicons-megaphone'
            );
        } else {
            add_management_page(
                    esc_html__( 'Dashboard Status Messages', 'dashboard-status-messages' ),
                    esc_html__( 'Status Messages', 'dashboard-status-messages' ),
                    'manage_options',
                    'dashboard-status-messages',
                    array( $this, 'admin_page' )
            );
        }
    }

    /**
     * Add setting link to the plugin
     * 
     * @links html
     * 
     * @return html
     */
    public function add_plguin_setting_link( $links ) {

        // Get base url.
        $base_url = $this->is_network_admin ? network_admin_url( 'admin.php' ) : admin_url( 'tools.php' );

        // Get page.
        $tools_url = add_query_arg(
                array(
                    'page' => 'dashboard-status-messages',
                ),
                $base_url
        );

        /**
         * Filter to modify page url used to status message dashbaord
         *
         * @param string $tools_url    URL
         *
         * @since 1.0.0
         */
        $tools_url = apply_filters( 'dsm_plugin_setting_page_url', $tools_url );
        $settings_link = '<a href="' . esc_url( $tools_url ) . '">' . esc_html__( 'Settings', 'dashboard-status-messages' ) . '</a>';
        array_push( $links, $settings_link );
        return $links;
    }

    /**
     * Status Management page under tools menu.
     * 
     * @since 1.0.0
     */
    public function admin_page() {
        $notice = $this->model_instance::is_network_active() ? get_blog_option( $this->current_blog_id, 'dsm_status_message' ) : get_option( 'dsm_status_message' );
        $editor_data = $this->model_instance->status_message_editor( $notice );

        //Loading status management page from view.
        require_once WPMU_DSM_DIR . 'views/admin/status-management-page.php';
    }

    /**
     * Enqueue styles
     * 
     * @since 1.0.0
     */
    public function enqueue_styles() {
        wp_enqueue_style( 'dashboard-status-massages', WPMU_DSM_URL . 'views/admin/css/dashboard-status-massages.css', array(), WPMU_DSM_VERSION, 'all' );
    }

    /**
     * Enqueue scripts
     * 
     * @since 1.0.0
     */
    public function enqueue_scripts() {
        wp_enqueue_script( 'dashboard-status-massages', WPMU_DSM_URL . 'views/admin/js/dashboard-status-massages.js', array( 'jquery', 'wp-tinymce' ), WPMU_DSM_VERSION, false );
    }

    /**
     * Add a new dashboard widget.
     * 
     * @since 1.0.0
     */
    public function admin_notice() {
        $notice = $this->model_instance::is_network_active() ? get_blog_option( $this->current_blog_id, 'dsm_status_message' ) : get_option( 'dsm_status_message' );
        if ( $notice ) {

            $editor_data = $this->model_instance->status_message_editor( $notice );

            //Loading admin notice from view.
            require_once WPMU_DSM_DIR . 'views/admin/admin-notices.php';
        }
    }

    /**
     * Add a new dashboard widget.
     * 
     * @since 1.0.0
     */
    public function add_dashboard_widgets() {
        wp_add_dashboard_widget( 'dashboard_widget', 'Status Message', array( $this, 'dashboard_widget_render' ), 'side', 'high' );
    }

    /**
     * Output the contents of the dashboard widget
     * 
     * @param Object $post current screen object
     * @param array $callback_args argument array
     * 
     * @since 1.0.0
     */
    public function dashboard_widget_render( $post, $callback_args ) {
        $notice = $this->model_instance::is_network_active() ? get_blog_option( $this->current_blog_id, 'dsm_status_message' ) : get_option( 'dsm_status_message' );
        if ( $notice ) {

            $editor_data = $this->model_instance->status_message_editor( $notice );

            //Loading dashboard notice from view.
            require_once WPMU_DSM_DIR . 'views/admin/dashboard-widgets.php';
        }
    }

}
