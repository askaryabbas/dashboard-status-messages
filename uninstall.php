<?php

/**
 * Dashboard Status Messages Uninstall methods
 * Called when plugin is deleted
 *
 * @since 1.0.0
 */

// If uninstall.php is not called by WordPress, die.
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
    die;
}

/**
 * Delete Dashboard Status Messages plugin option `dsm_status_message` for both single site and multi-site.
 */
if ( ! is_multisite() ) {
    delete_option( 'dsm_status_message' );
} else {
    $blog_ids = get_sites( array( 'fields' => 'ids' ) );
    foreach ( $blog_ids as $blog_id ) {
        delete_blog_option( $blog_id, 'dsm_status_message' );
    }
}